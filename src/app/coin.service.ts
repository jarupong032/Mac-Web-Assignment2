import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CoinService {

  result: any;
  constructor(private http: HttpClient) {}

  addCoin(name, price , email , phone) {
    const uri = 'https://xsintern-api.learnbalance.com/api/users';
    const obj = {
      user_firstname: name,
      user_lastname: price,
      user_email: email,
      user_phone: phone
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res =>
          console.log('Done'));
  }

  getCoins() {
    const uri = 'https://xsintern-api.learnbalance.com/api/users';
    return this
            .http
            .get(uri)
            .map(res => {
              return res;
            });
  }

  editCoin(id) {
    const uri = 'https://xsintern-api.learnbalance.com/api/users/' + id;
    return this
            .http
            .get(uri)
            .map(res => {
              return res;
            });
  }

  updateCoin(name, price, email, phone, id) {
    const uri = 'https://xsintern-api.learnbalance.com/api/users/' + id;

    const obj = {
      user_firstname: name,
      user_lastname: price,
      user_email: email,
      user_phone: phone
    };
    this
      .http
      .put(uri, obj)
      .subscribe(res => console.log('Done'));
  }

  deleteCoin(id) {
    const uri = 'https://xsintern-api.learnbalance.com/api/users/' + id;

        return this
            .http
            .delete(uri)
            .map(res => {
              return res;
            });
  }
}