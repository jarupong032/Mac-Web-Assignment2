import { Component, OnInit } from '@angular/core';
import { CoinService } from '../coin.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  title = 'Add Coin';
  angForm: FormGroup;
  constructor(private coinservice: CoinService, private fb: FormBuilder) {
    this.createForm();
   }
  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      price: ['', Validators.required ],
      email: ['', Validators.required ],
      phone: ['', Validators.required ]
   });
  }
  addCoin(name, price, email, phone) {
      this.coinservice.addCoin(name, price, email, phone);
      
  }
  ngOnInit() {
  }
}